# Pastebin crawler

To use this crawler, run the docker service bundle using the following command:

```bash
docker-compose up -d    # start services
docker-compose logs -f  # show logs
```

You can update the filters (`PATTERNS` constant) in the [config.py](data/crawler/config.py) file.

Here is a detailed example:

```json
{
    'title': 'Password',
    'output': 'loot/password.txt',
    'regex': r'(?:password\b|pass\b|pswd\b|passwd\b|pwd\b|pass\b)[:\- \t="\']*(?P<loot>(?P<password>[^:\- \t\n\r"\']+)?)'
}
```

This pattern is used to search for a password in the collected pastes using the `regex`. The named-group `loot` is used to select the data to be dumped to the output file.

Basically, the following paste:

```
I managed to get the password "helloThere" from my neighbor.
```

Will give us the following output:

![match](img/match.png)

