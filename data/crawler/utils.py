#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class BannedIPException(Exception):
    pass

class ExpiredPasteException(Exception):
    pass