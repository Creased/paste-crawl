#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from config import (AMQP_URL, EXCHANGE, PATTERNS)
from utils import (BannedIPException, ExpiredPasteException)

from fake_useragent import UserAgent
from bs4 import BeautifulSoup

import traceback
import requests
import time
import pika
import sys
import re

pastes = []

def parse(paste):
    result = True
    print(f'-- {paste} --')

    try:
        # headers = {'User-Agent': UserAgent().random}
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'}
        uri = f'https://pastebin.com/raw/{paste}'
        text = requests.get(f'https://pastebin.com/raw/{paste}', headers=headers).text

        if 'blocked you' not in text:
            if 'page is no longer available' not in text:
                for pattern in PATTERNS:
                    matches = re.finditer(pattern['regex'], text, re.MULTILINE|re.DOTALL)
                    for match in matches:
                        print(f'Found {pattern["title"]}:')
                        for info in match.groupdict():
                            if info != 'loot':
                                print(f'> {info}: {match.groupdict()[info]}')
                        with open(pattern['output'], 'a+') as fd:
                            fd.write(f'{match.groupdict()["loot"]}\n')
            else:
                raise ExpiredPasteException
        else:
            raise BannedIPException
    except ExpiredPasteException:
        print('Paste is expired.')
        result = False
    except BannedIPException:
        print('IP got blocked... Waiting for 5 minutes.')
        time.sleep(5*60)
        result = False
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        result = False

    return result

def callback(ch, method, properties, body):
    paste = body.decode()

    if paste not in pastes:
        result = parse(paste)
        if result:
            pastes.append(paste)

def main():
    try:
        with pika.BlockingConnection(pika.URLParameters(AMQP_URL)) as connection:
            print(f'Connected to AMQP broker.')

            channel = connection.channel()
            channel.exchange_declare(exchange=EXCHANGE, exchange_type='fanout')

            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue

            channel.queue_bind(exchange=EXCHANGE, queue=queue_name)

            channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

            channel.start_consuming()
    except pika.exceptions.AMQPConnectionError:
        print('AMQP connection error... Retrying in 5 seconds.')
        time.sleep(5)

if __name__ == '__main__':
    while True:
        main()