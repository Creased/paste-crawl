#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from config import (AMQP_URL, EXCHANGE, ROUTING_KEY, DELAY)
from utils import (BannedIPException)

from fake_useragent import UserAgent
from bs4 import BeautifulSoup

import traceback
import requests
import time
import pika
import sys

pastes = []

def get_new_pastes():
    new_pastes = []
    try:
        # headers = {'User-Agent': UserAgent().random}
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'}
        text = requests.get('https://pastebin.com/archive', headers=headers).text
        if 'blocked you' not in text:
            html = BeautifulSoup(text, 'html.parser')
            links = html.findAll('table', {'class':'maintable'})[0].findAll('a')
        else:
            raise BannedIPException
    except BannedIPException:
        print('IP got blocked... Waiting for 5 minutes.')
        time.sleep(5*60)
    except IndexError:
        print('An error occured while getting new pastes...')
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
    else:
        for link in links:
            paste = link['href'].split('/')[1]
            if paste not in pastes:
                pastes.append(paste)
                new_pastes.append(paste)
    finally:
        return new_pastes

def main():
    with pika.BlockingConnection(pika.URLParameters(AMQP_URL)) as connection:
        print(f'Connected to AMQP broker.')

        channel = connection.channel()
        channel.exchange_declare(exchange=EXCHANGE, exchange_type='fanout')

        ## Get new pastes.
        while True:
            new_pastes = get_new_pastes()

            if new_pastes:
                print(f'New pastes: {new_pastes}')
                for paste in new_pastes:
                    channel.basic_publish(exchange=EXCHANGE, routing_key=ROUTING_KEY, body=paste)

            time.sleep(DELAY)

if __name__ == '__main__':
    main()