#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import environ

# environment variables.
AMQP_URL = environ.get('AMQP_URL', 'amqp://rabbitmq?connection_attempts=5&retry_delay=20')
EXCHANGE = 'pastes'
ROUTING_KEY = ''
DELAY = 5
PATTERNS = [
    {
        'title': 'URI',
        'output': 'loot/uri.txt',
        'regex': r'(?P<loot>(?P<scheme>https?):[\/]{1,2}(?P<host>[^\/ \n\r"\']+)?\/(?P<path>[^\/ \n\r"\']+)?)'
    },
    {
        'title': 'FTP info',
        'output': 'loot/ftp.txt',
        'regex': r'(?P<loot>ftp:[\/]{1,2}((?P<username>[^:]+):(?P<password>[^@]+)@)?(?P<host>[^\/]+)?\/(?P<path>[^\/ \n\r"\']+)?)'
    },
    {
        'title': 'Password',
        'output': 'loot/password.txt',
        'regex': r'(?:password\b|pass\b|pswd\b|passwd\b|pwd\b|pass\b)[:\- \t="\']*(?P<loot>(?P<password>[^:\- \t\n\r"\']+)?)'
    }
]